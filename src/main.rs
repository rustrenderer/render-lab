extern crate sp_gui;
extern crate sp_renderer;

mod application;

use application::Application;

fn main() {
    let app = Application::new();

    app.run();
}
