#version 410 core

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coord;

out VS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} vs_out;

void main() {
    vs_out.pos = in_pos;
    vs_out.normal = in_normal;
    vs_out.uv = in_tex_coord;
}