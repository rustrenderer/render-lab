#version 410 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

uniform bool soft_shading;
uniform vec2 window_size;
uniform mat4 mvp;

in TESE_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} gs_in[];

out GS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} gs_out;

noperspective out vec3 dist;

void main() {
    vec3 face_normal = normalize(cross(
        gs_in[1].pos - gs_in[0].pos,
        gs_in[2].pos - gs_in[0].pos
    ));

    vec4 _p0 = mvp * vec4(gs_in[0].pos, 1.0);
    vec4 _p1 = mvp * vec4(gs_in[1].pos, 1.0);
    vec4 _p2 = mvp * vec4(gs_in[2].pos, 1.0);

    vec2 p0 = window_size * _p0.xy/_p0.w;
    vec2 p1 = window_size * _p1.xy/_p1.w;
    vec2 p2 = window_size * _p2.xy/_p2.w;
    vec2 v0 = p2-p1;
    vec2 v1 = p2-p0;
    vec2 v2 = p1-p0;
    float area = abs(v1.x * v2.y - v1.y * v2.x);
    float h1 = area/length(v0);
    float h2 = area/length(v1);
    float h3 = area/length(v2);

    gl_Position = gl_in[0].gl_Position;
    gs_out.pos = gs_in[0].pos;
    gs_out.normal = soft_shading ? gs_in[0].normal : face_normal;
    gs_out.uv = gs_in[0].uv;
    dist = vec3(h1, 0, 0);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    gs_out.pos = gs_in[1].pos;
    gs_out.normal = soft_shading ? gs_in[1].normal : face_normal;
    gs_out.uv = gs_in[1].uv;
    dist = vec3(0, h2, 0);
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    gs_out.pos = gs_in[2].pos;
    gs_out.normal = soft_shading ? gs_in[2].normal : face_normal;
    gs_out.uv = gs_in[2].uv;
    dist = vec3(0, 0, h3);
    EmitVertex();

    EndPrimitive();
}