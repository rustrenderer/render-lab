#version 410 core

layout(triangles, equal_spacing, ccw) in;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
    return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
    return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 mvp;

in TESC_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} tese_in[];

out TESE_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} tese_out;

void main ()
{
    vec3 in_pos = interpolate3D(tese_in[0].pos, tese_in[1].pos, tese_in[2].pos);
    vec3 in_normal = interpolate3D(tese_in[0].normal, tese_in[1].normal, tese_in[2].normal);
    vec2 in_uv = interpolate2D(tese_in[0].uv, tese_in[1].uv, tese_in[2].uv);

    tese_out.pos = (model * vec4(in_pos, 1.0)).xyz;
    tese_out.normal = in_normal;
    tese_out.uv = in_uv;

    gl_Position = mvp * vec4(in_pos, 1.0);
}