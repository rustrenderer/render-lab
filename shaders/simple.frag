#version 410 core

in GS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} fs_in;

noperspective in vec3 dist;

uniform bool draw_wireframe;

uniform float specular_coefficient;
uniform float alpha;

uniform vec3 color_ambient;
uniform vec3 color_diffuse;
uniform vec3 color_specular;

void main ()
{
    float nearD = min(min(dist.x,dist.y),dist.z);
    float edgeIntensity =  max(min(1.0, exp2(-1.0*nearD*nearD)), 0.0);

    vec3 light_pos = vec3(5);
    vec3 light_dir = normalize(light_pos - fs_in.pos);
    vec3 light_color = vec3(1);

    float diff = max(dot(normalize(fs_in.normal), light_dir), 0.0);


    vec4 diffuse = vec4(color_diffuse * diff * 0.9, 1.0);
    vec4 ambient = vec4(color_ambient * 0.1, 1.0);

    vec4 color = (diffuse + ambient) * vec4(color_diffuse, alpha);

    if (draw_wireframe)
        gl_FragColor = edgeIntensity * vec4(1.0) + (1.0 - edgeIntensity) * color;
    else
        gl_FragColor = color;
}
